var args = arguments[0] || {};
// in mill. Minimum Time between two scroll event. Greater value save CPU,
// but no smoth animation
const WAIT_SCROLL_TIME = 40;

// xml boolean args is string ("false" == true)
_.each(['tabs', 'findScrollableView'], function(key) {

    if (!_.has(args, key)) {
        return;
    }

    try {
        args[key] = JSON.parse(args[key]);
    } catch (e) {
        delete args[key];
        Ti.API.error("Unable to set argument '" + key + "'. It must be boolean.");
    }
});

// fill undefined args with defaults
_.defaults(args, {
    bottomColor: "#ededed",
    dividerColor: "#ccc",
    indicatorColor: "#000",
    indicatorHeight: 5,
    tabs: false,
    scrollOffset: 40,
    height: args.tabs ? 48 : 5,
    width: Ti.UI.FILL,
    findScrollableView: true,
    color: "#000",
    font: {
        fontSize: 13,
        fontWeight: 'bold'
    },
    fitIndicatorWidth: true,
    padding: 8,
    textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
    animation: true
});

// additional adjustments for tabs
if (args.tabs) {
    args.tabs = {
        dividerColor: args.dividerColor,
        width: args.tabWidth,
        backgroundColor: args.backgroundColor,
        activeColor: args.activeColor,
        color: args.color,
        font: args.font,
        padding: args.padding,
        textAlign: args.textAlign,
        fitWidth: args.fitWidth
    };
}

// apply properties of Ti.UI.View that can be applied to paging control view
[
    "backgroundColor",
    "backgroundImage",
    "backgroundLeftCap",
    "backgroundRepeat",
    "backgroundTopCap",
    "borderRadius",
    "borderWidth",
    "bottom",
    "height",
    "horizontalWrap",
    "left",
    "opacity",
    "right",
    "top",
    "visible",
    "width",
    "zIndex"
].forEach(function(prop) {
    _.has(args, prop) && ($.pagingcontrol[prop] = args[prop]);
});

// NOTE: THIS DOES NOT WORK ANYMORE WITH ALLOY 1.4.0
// try to find scrollable view as child of parent
// this should work, if pagingcontrol is scrollable view have the same parent
// otherwise you can pass it with args or setScrollableView
if (args.__parentSymbol && args.findScrollableView) {
    args.__parentSymbol.children.length > 0 &&
        ($.scrollableView = _.find(args.__parentSymbol.children, function(child) {
            return child.apiName === "Ti.UI.ScrollableView";
        }));
}

// assign passed reference of scrollable view
(_.has(args, "scrollableView")) && ($.scrollableView = args.scrollableView);

if ($.scrollableView) {
    postLayout(init);
}

/**
 * calls back after postlayout
 * @param {Function} callback
 * @param {Boolean} orientation change  wether called from oc callback
 */
function postLayout(callback, oc) {

    if (!oc && $.pagingcontrol.size.width) {
        callback();
    } else {

        // wait for postlayout event to get the pagingcontrol size
        $.pagingcontrol.addEventListener('postlayout', function onPostLayout(evt) {

            // callback
            callback();

            // remove eventlistener
            evt.source.removeEventListener('postlayout', onPostLayout);
        });
    }
}

var currentTab = null;

/**
 * initialization method
 */
function init() {

    if (args.tabs) {

        // create tabs
        $.tabsCtrl = Widget.createController('tabs', {
            tabs: args.tabs,
            titles: _.map($.scrollableView.views, function(v) {
                return v.title;
            })
        });



        // add tabs
        $.pagingcontrol.add($.tabsCtrl.getView());

        // add bottom border
        /*$.pagingcontrol.add(Ti.UI.createView({
          width: Ti.UI.FILL,
          height: 2,
          bottom: 0,
          backgroundColor: args.bottomColor
        }));*/

        // add tab select listener
        $.tabsCtrl.on('select', function(e) {

            if (currentTab !== e.tab) {
                currentTab = e.tab;

                $.trigger('select', {
                    tab: e.tab,
                    view: e.view
                });
                $.scrollableView.currentPage = e.tab;

                // update the indicator position
                updateIndicatorPosition(false, e.tab);

                args.tabs && updateOffset(e.tab);

                onScrollEnd({
                    currentPage: e.tab
                });
            }


        });
    }

    // create the indicator view
    $.indicator = Ti.UI.createView({
        //backgroundColor: args.indicatorColor,
        backgroundColor: 'transparent',
        height: args.indicatorHeight,
        width: args.fitIndicatorWidth ? $.tabsCtrl.tabWidth : '50%',
        top: 0,
        zIndex: 2
    });

    $.indicator.add(Ti.UI.createView({
        top: 0,
        bottom: 0,
        backgroundColor: args.indicatorColor,
        id: 'indicator'
    }));

    adjustePositions();

    // add the indicator
    $.pagingcontrol.add($.indicator);

    // add scroll listener to scrollable view
    $.scrollableView.addEventListener('scroll', onScroll);
    $.scrollableView.addEventListener('scrollend', onScrollEnd);
    Ti.Gesture.addEventListener('orientationchange', onOrientationChange);

}

/**
 * Callback for scroll event
 */
function handleScroll(e) {

    // restrict this to $.scrollableView to support nesting scrollableViews
    if (e.source !== $.scrollableView)
        return;

    // update the indicator position
    updateIndicatorPosition(false, e.currentPageAsFloat);

    if (currentTab) {
        currentTab = null;
    }

    args.tabs && updateOffset(e.currentPageAsFloat);
}
/** Set Max excution time to save CPU **/
var onScroll = _.throttle(handleScroll, WAIT_SCROLL_TIME);

/**
 * Callback for scrollend event
 */
var previousPage = null;

function onScrollEnd(e) {
    if (previousPage !== e.currentPage) {
        $.tabsCtrl.selectColor(e.currentPage);
        previousPage = e.currentPage;
    }
}

// Use to cache the correcte pagingcontrol width == screen size for max val
// But on IOS, when recalculate view ( background to foreground ), the size is the same has content ( > screen size)
// which create issus when calculate the offset of the  pagingcontrol (the scrollview)
var screenSize = 0;

/**
 * sets the tab bar offset
 * @param {Number} index
 */
function updateOffset(index) {

    if (args.tabWidth === 'auto') {
        return;
    }

    var width = screenSize || $.pagingcontrol.size.width,
        tabsWidth = $.tabsCtrl.getWidth(),
        maxOffset = tabsWidth - width,
        tabSpace = tabsWidth * index / $.scrollableView.views.length;

    if (width < tabsWidth) {

        if (!screenSize)
            screenSize = $.pagingcontrol.size.width;

        var offset = tabSpace - args.scrollOffset,
            offsetDp = offset < maxOffset ? offset : maxOffset,
            newOffset = OS_IOS ? (offsetDp < 0 ? 0 : offsetDp) : offsetDp;

        $.pagingcontrol.setContentOffset({
            x: newOffset,
            y: 0
        }, {
            animated: false
        });

    }
}

/**
 * Callback for orientationchange event
 */
function onOrientationChange(e) {

    if (OS_IOS) {

        $.tabsCtrl.updateWidth();

        adjustePositions();

        if (currentTab) {
            currentTab = null;
        }
        var currentPage = $.scrollableView.currentPage;

        args.tabs && updateOffset(currentPage);
    } else {
        postLayout(function() {
            $.tabsCtrl.updateWidth();

            adjustePositions();
        }, true);
    }

}

function updateIndicatorPosition(updateWidth, position) {

    if (position === undefined) { // || !args.animation) {
        position = $.scrollableView.currentPage;
    }

    var width = calculateWidthIndic(position) || $.iWidth / 2,
        offset = (($.iWidth) - width) / 2,
        newLeft = position * ($.iWidth) + offset,
        needChange = ($.indicator.width != width),
        needChangePosition = ($.indicator.left != newLeft);

    needChange && $.indicator.setWidth(width);
    if (needChangePosition || args.animation) $.indicator.left = newLeft;

}

function calculateWidthIndic(position) {
    if (args.fitIndicatorWidth) {
        var leftIndex = Math.floor(position),
            rightIndex = Math.ceil(position);

        var leftTab = $.tabsCtrl && $.tabsCtrl.getTabView(leftIndex);
        var rightTab = ((leftIndex != rightIndex) ? $.tabsCtrl.getTabView(rightIndex) : null);

        if (rightTab == null) {
            return leftTab.rect.width;
        } else {
            //do some Math when tab is between 2 postions
            var coef = position - leftIndex,
                diff = rightTab.rect.width - leftTab.rect.width,
                realDiff = coef * diff,
                result = leftTab.rect.width + realDiff;

            return result;
        }

    } else {
        return $.iWidth / 2;
    }
}

/**
 * Adjust initial layout positions
 */
function adjustePositions() {
    var totalWidth = args.tabs ? $.tabsCtrl.getWidth() : $.pagingcontrol.size.width;
    $.iWidth = Math.floor(totalWidth / $.scrollableView.views.length);
    updateIndicatorPosition(true);
}


/**
 * update the paging control if views were added / removed from scrollableView
 */
exports.refresh = function() {
    $.pagingcontrol.removeAllChildren();
    exports.cleanup();
    init();
};

/**
 * if you need to set it in the controller
 * @param {Ti.UI.Scrollableview} scrollable view
 */
exports.setScrollableView = function(_sv) {
    if ($.scrollableView) {
        Ti.API.error("Already initialized");
        return;
    }
    $.scrollableView = _sv;
    postLayout(init);
};

/**
 * removes orientationchange Listener
 */
exports.cleanup = function() {
    $.scrollableView.removeEventListener('scroll', onScroll);
    $.scrollableView.removeEventListener('scrollend', onScrollEnd);
    Ti.Gesture.removeEventListener('orientationchange', onOrientationChange);
    args.tabs && $.tabsCtrl && $.tabsCtrl.off() && ($.tabsCtrl = null);
    $.indicator = null;
};